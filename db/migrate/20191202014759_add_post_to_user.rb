class AddPostToUser < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :post, null: true, foreign_key: true
  end
end
