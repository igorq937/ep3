# Ep3

# Requisitos

* Ruby 2.6.3
* Rails 6.0.1
* [ImageMagick](https://imagemagick.org/index.php)

# Para utilizar o projeto:

Caso não possua o ImageMagick:

```bash
$ sudo apt-get install imagemagick -y
```

***

Execute no bash para clonar e preparar o projeto:

```bash
$ git clone https://gitlab.com/igorq937/ep3.git
$ cd ep3
$ yarn install --check-files
$ gem install bundler
$ bundle install
$ rails db:migrate
```

***

Para criar o primeiro usuário administrador:

```bash
$ rake db:seed
```

O administrador terá ``email = "admin@email.com"`` e ``senha = "admin1"``

Que pode ser alterada via interface do site ou utilizando o ```rails console```

***

Para iniciar o servidor:

```bash
$ rails serve
```

# Objetivo:

O objetivo desse projeto é facilitar doação e arrecadação de cadeiras de rodas e afins a quem necessita.

# Funcionalidades:

* Gerenciar posts de itens para doação.
* Listar itens que estão sendo doados. (10 por página)

# Permissões:

* Visitantes: Podem visualizar os posts de doação.
* Cadastrados: Podem criar e gerenciar seus próprios posts.
* Administrador: Pode destruir qualquer post.

As permissões podem ser redefinidas no arquivo [ability.rb](app/models/ability.rb), acesse [CanCanCan](https://github.com/CanCanCommunity/cancancan/wiki/defining-abilities) para saber mais.

# Gems utilizadas:

* gem 'paperclip'
* gem 'bootstrap'
* gem 'jquery-rails'
* gem 'simple_form'
* gem 'devise'
* gem 'cancancan''
* gem 'will_paginate'
* gem 'will_paginate-bootstrap4'
