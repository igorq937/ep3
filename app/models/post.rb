class Post < ApplicationRecord
  validates :title, :body, presence: true

  has_attached_file :image, styles: { medium: "400x300>", thumb: "286x180>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  belongs_to :user

  self.per_page = 10

  scope :search, ->(query) { where("title like ?", "%#{query}%") }
  scope :myposts, ->(id) { where("user_id like ?", "%#{id}%") }
end
