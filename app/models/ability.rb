# frozen_string_literal: true

class Ability
  include CanCan::Ability
  
  def initialize(user)

    can :read, Post

    if user.present?
      can :create, Post
      can [:update, :destroy], Post, user_id: user.id 
      if user.admin?
        can :destroy, Post
      end
    end
    
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
