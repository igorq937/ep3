class ApplicationController < ActionController::Base

    before_action :configure_permitted_parameters, if: :devise_controller?

    protected

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:admin, :nick, :phoneNumber, :zipcode, :state, :city, :neighborhood, :whatsapp, :telegram])
        devise_parameter_sanitizer.permit(:account_update, keys: [:admin, :nick, :phoneNumber, :zipcode, :state, :city, :neighborhood, :whatsapp, :telegram])
    end

end
